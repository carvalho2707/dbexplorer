CREATE TABLE if not exists controlo
(
    id        varchar(250) not null primary key ,
    jan       DECIMAL      NULL DEFAULT 0.0,
    fev       DECIMAL      NULL DEFAULT 0.0,
    mar       DECIMAL      NULL DEFAULT 0.0,
    jun       DECIMAL      NULL DEFAULT 0.0,
    jul       DECIMAL      NULL DEFAULT 0.0,
    aug       DECIMAL      NULL DEFAULT 0.0,
    sep       DECIMAL      NULL DEFAULT 0.0,
    oct       DECIMAL      NULL DEFAULT 0.0,
    nov       DECIMAL      NULL DEFAULT 0.0,
    dec       DECIMAL      NULL DEFAULT 0.0
);