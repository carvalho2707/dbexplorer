package pt.tiagocarvalho.dbexplorerui.exceptions

class DatabaseNotFoundException(message: String?) : Exception(message)