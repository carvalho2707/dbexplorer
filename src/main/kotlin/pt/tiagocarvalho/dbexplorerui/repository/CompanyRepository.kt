package pt.tiagocarvalho.dbexplorerui.repository

import org.springframework.stereotype.Service
import java.sql.ResultSet
import java.sql.Statement

@Suppress("SqlResolve")
@Service
class CompanyRepository : BaseClientRepository() {

    fun getNameAndVat(clientNumber: String, ano: Int): Pair<String, String> {
        val connection = getConnection(clientNumber, ano)
        val statement: Statement = connection.createStatement()
        val query = "select CPar, Valor from ParEmp where CPar='Empresa'  or Cpar='ncontrib'"
        var rs: ResultSet? = null
        var name = ""
        var vat = ""
        try {
            rs = statement.executeQuery(query)
            while (rs.next()) {
                val cpar = rs.getString("CPar")
                val value = rs.getString("Valor")
                if (cpar == "Empresa")
                    name = value
                else vat = value
            }
        } catch (e: Exception) {

        } finally {
            if (rs != null && rs.isClosed.not()) {
                rs.close()
            }

            statement.close()
            connection.close()
        }
        return Pair(name, vat)
    }
}