package pt.tiagocarvalho.dbexplorerui.repository

import org.springframework.beans.factory.annotation.Autowired
import pt.tiagocarvalho.dbexplorerui.conf.ConfigurationProperties
import pt.tiagocarvalho.dbexplorerui.exceptions.DatabaseNotFoundException
import java.sql.Connection
import java.sql.DriverManager

abstract class BaseClientRepository {

    @Autowired
    lateinit var configurationProperties: ConfigurationProperties

    private fun getDbName(clientNumber: String, ano: Int): String {
        return clientNumber.toIntOrNull()?.let {
            if (it in 458..520) {
                when (it) {
                    468 -> "${clientNumber}IMO${ano}ACtb"
                    471, 478, 502, 513 -> "${clientNumber}${ano}ACtb"
                    else -> "${clientNumber}CTB${ano}ACtb"
                }
            } else {
                "${clientNumber}${ano}ACtb"
            }
        } ?: throw DatabaseNotFoundException("Invalid database name")
    }

    protected fun getConnection(clientNumber: String, ano: Int): Connection {
        val dbName = getDbName(clientNumber, ano)
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
        val connectionUrl = "${configurationProperties.db.url};" +
            "database=${dbName};" +
            "user=${configurationProperties.db.username};" +
            "password=${configurationProperties.db.password};" +
            "encrypt=true;" +
            "trustServerCertificate=true;"
        return DriverManager.getConnection(connectionUrl)
    }
}