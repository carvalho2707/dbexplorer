package pt.tiagocarvalho.dbexplorerui.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pt.tiagocarvalho.dbexplorerui.entities.Controlo

@Repository
interface ControloRepository : JpaRepository<Controlo, String>