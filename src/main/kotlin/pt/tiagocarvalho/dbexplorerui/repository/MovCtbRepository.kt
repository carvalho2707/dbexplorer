package pt.tiagocarvalho.dbexplorerui.repository

import org.springframework.stereotype.Service
import pt.tiagocarvalho.dbexplorerui.exceptions.DatabaseNotFoundException
import pt.tiagocarvalho.dbexplorerui.model.Mov
import pt.tiagocarvalho.dbexplorerui.model.SearchVO
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement

@Service
class MovCtbRepository : BaseClientRepository() {

    fun getMovCtb(searchVO: SearchVO): List<Mov> {
        val result = mutableListOf<Mov>()

        val connection: Connection
        try {
            connection = getConnection(searchVO.clientNumber, searchVO.ano)
        } catch (sqle: SQLException) {
            sqle.printStackTrace()
            throw DatabaseNotFoundException(sqle.message)
        }

        val statement: Statement = connection.createStatement()
        val query = "SELECT ccONTA, nmES, data, TDoc, ValM, D_C, NMes " +
            "FROM lnmov " +
            "WHERE NMes < ${searchVO.mes + 1} " +
            "AND (Cconta like '31%' OR Cconta like '6%' OR Cconta like '7%' OR Cconta like '38%' OR (NMes = '0' AND (Cconta like '32%' OR CConta like '33%')) OR " +
            "(Cconta like '27222%' AND NMes = '0'))"
        var rs: ResultSet? = null

        try {
            rs = statement.executeQuery(query)
            while (rs.next()) {
                val conta = rs.getString("ccONTA")
                val id = generateId(conta)
                val mes = rs.getInt("nmES")
                val data = rs.getTimestamp("data")
                val tDoc = rs.getInt("TDoc")
                val valor = rs.getBigDecimal("ValM")
                val cDoc = rs.getString("D_C")
                val nMes = rs.getInt("NMes")
                result.add(Mov(id, conta, mes, data.toLocalDateTime(), tDoc, valor, cDoc, nMes))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (rs != null && rs.isClosed.not()) {
                rs.close()
            }

            statement.close()
            connection.close()
        }
        return result
    }

    private fun generateId(conta: String) = when {
        conta.startsWith("33") -> conta.substring(0, 3)
        conta.startsWith("32") -> conta.substring(0, 3)
        else -> conta.substring(0, 2)
    }
}