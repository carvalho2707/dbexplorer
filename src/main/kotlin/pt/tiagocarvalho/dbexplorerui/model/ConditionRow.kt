package pt.tiagocarvalho.dbexplorerui.model

data class ConditionRow(
    val label: String,
    val value: String
)