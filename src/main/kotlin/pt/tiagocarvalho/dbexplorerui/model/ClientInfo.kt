package pt.tiagocarvalho.dbexplorerui.model

data class ClientInfo(
    val code: String,
    val name: String
)