package pt.tiagocarvalho.dbexplorerui.model

import java.math.BigDecimal
import java.time.LocalDate

data class SearchVO(
    val clientNumber: String,
    val ano: Int = LocalDate.now().year,
    val mes: Int = LocalDate.now().monthValue,
    val margemComerConta71: BigDecimal,
    val margemComerConta72: BigDecimal,
    val margemConsumoConta72: BigDecimal,
    val amortizacoes: BigDecimal
)