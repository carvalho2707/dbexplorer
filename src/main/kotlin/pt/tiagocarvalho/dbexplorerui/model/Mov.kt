package pt.tiagocarvalho.dbexplorerui.model

import java.math.BigDecimal
import java.time.LocalDateTime

data class Mov(
    val id: String,
    val conta: String,
    val mes: Int,
    val data: LocalDateTime,
    val tDoc: Int,
    val valor: BigDecimal,
    val cDoc: String,
    val mesAux: Int
)