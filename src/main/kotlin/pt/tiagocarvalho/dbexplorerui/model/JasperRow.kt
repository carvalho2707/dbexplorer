package pt.tiagocarvalho.dbexplorerui.model

data class JasperRow(
    val ct: String,
    val des: String,
    val jan: String,
    val feb: String,
    val mar: String,
    val apr: String,
    val may: String,
    val jun: String,
    val jul: String,
    val aug: String,
    val sep: String,
    val oct: String,
    val nov: String,
    val dec: String,
    val tot: String,
    val firstRow: Boolean,
    val lastRow: Boolean,
    val totalRow: Boolean,
    val removeTotalBackground: Boolean
)