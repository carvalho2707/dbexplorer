package pt.tiagocarvalho.dbexplorerui.model

enum class ReportType(val extension: String) {
    PDF("pdf"), XLSX("xls")
}