package pt.tiagocarvalho.dbexplorerui.model

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import pt.tiagocarvalho.dbexplorerui.utils.toSimpleStringProperty
import java.math.BigDecimal

data class TableRowInfo(
    val ct: StringProperty,
    val designacao: StringProperty,
    val hasBackground: Boolean,
    val jan: StringProperty,
    val feb: StringProperty,
    val mar: StringProperty,
    val apr: StringProperty,
    val may: StringProperty,
    val jun: StringProperty,
    val jul: StringProperty,
    val aug: StringProperty,
    val sep: StringProperty,
    val oct: StringProperty,
    val nov: StringProperty,
    val dec: StringProperty,
    val tot: StringProperty,
    val isTotal: Boolean,
    val removeTotalBackground: Boolean
) {

    constructor(removeTotalBackground: Boolean) : this(
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        false,
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        SimpleStringProperty(""),
        false,
        removeTotalBackground
    )

    constructor(
        ct: String,
        designacao: String,
        hasBackground: Boolean,
        jan: BigDecimal,
        feb: BigDecimal,
        mar: BigDecimal,
        apr: BigDecimal,
        may: BigDecimal,
        jun: BigDecimal,
        jul: BigDecimal,
        aug: BigDecimal,
        sep: BigDecimal,
        oct: BigDecimal,
        nov: BigDecimal,
        dec: BigDecimal,
        tot: BigDecimal,
        isTotal: Boolean,
        removeTotalBackground: Boolean
    ) : this(
        SimpleStringProperty(ct),
        SimpleStringProperty(designacao),
        hasBackground,
        jan.toSimpleStringProperty(),
        feb.toSimpleStringProperty(),
        mar.toSimpleStringProperty(),
        apr.toSimpleStringProperty(),
        may.toSimpleStringProperty(),
        jun.toSimpleStringProperty(),
        jul.toSimpleStringProperty(),
        aug.toSimpleStringProperty(),
        sep.toSimpleStringProperty(),
        oct.toSimpleStringProperty(),
        nov.toSimpleStringProperty(),
        dec.toSimpleStringProperty(),
        tot.toSimpleStringProperty(),
        isTotal,
        removeTotalBackground
    )
}