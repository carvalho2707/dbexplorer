package pt.tiagocarvalho.dbexplorerui.model

data class SaveExistencias(
    val clientNumber: String,
    var jan: String,
    var fev: String,
    var mar: String,
    var apr: String,
    var may: String,
    var jun: String,
    var jul: String,
    var aug: String,
    var sep: String,
    var oct: String,
    var nov: String,
    var dec: String
)