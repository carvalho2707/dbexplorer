package pt.tiagocarvalho.dbexplorerui.model

import java.math.BigDecimal

class MonthRecord(
    val categories: MutableMap<String, BigDecimal>
) {

    fun update(id: String, value: BigDecimal) {
        categories[id] = categories[id]!!.add(value)
    }
}