package pt.tiagocarvalho.dbexplorerui.model

data class ReportInput(
    val jasperRows: List<JasperRow>,
    val conditions: List<ConditionRow>,
    val month: String,
    val year: Int,
    val companyName: String,
    val companyVat: String,
    val code: String
)