package pt.tiagocarvalho.dbexplorerui.initializer

import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationListener
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import pt.tiagocarvalho.dbexplorerui.EnterApplication

@Component
class StageInitializer(
    @Value("\${spring.application.ui.title}") val applicationTitle: String,
    val applicationContext: ApplicationContext
) : ApplicationListener<EnterApplication.StageReadyEvent> {

    @Value("classpath:/landing.fxml")
    private lateinit var resource: Resource

    override fun onApplicationEvent(p0: EnterApplication.StageReadyEvent) {
        val fxmlLoader = FXMLLoader(resource.url)
        fxmlLoader.setControllerFactory { aClass -> applicationContext.getBean(aClass) }
        val parent: Parent = fxmlLoader.load()

        val stage = p0.getStage() as Stage
        stage.scene = Scene(parent)
        stage.icons.add(Image(javaClass.getResource("/images/icon.png").toExternalForm()))
        stage.title = applicationTitle
        stage.show()
    }
}