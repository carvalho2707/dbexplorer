package pt.tiagocarvalho.dbexplorerui.entities

import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Controlo(
    @Id
    var id: String? = null,
    @Column(nullable = false)
    var jan: BigDecimal,
    @Column(nullable = false)
    var fev: BigDecimal,
    @Column(nullable = false)
    var mar: BigDecimal,
    @Column(nullable = false)
    var apr: BigDecimal,
    @Column(nullable = false)
    var may: BigDecimal,
    @Column(nullable = false)
    var jun: BigDecimal,
    @Column(nullable = false)
    var jul: BigDecimal,
    @Column(nullable = false)
    var aug: BigDecimal,
    @Column(nullable = false)
    var sep: BigDecimal,
    @Column(nullable = false)
    var oct: BigDecimal,
    @Column(nullable = false)
    var nov: BigDecimal,
    @Column(nullable = false)
    var dec: BigDecimal
) {
    constructor(clientNumber: String) : this(
        clientNumber,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        BigDecimal.ZERO
    )
}