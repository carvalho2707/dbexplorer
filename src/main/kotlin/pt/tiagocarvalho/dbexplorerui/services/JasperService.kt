package pt.tiagocarvalho.dbexplorerui.services

import net.sf.jasperreports.engine.JREmptyDataSource
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import net.sf.jasperreports.engine.export.JRPdfExporter
import net.sf.jasperreports.engine.export.JRXlsExporter
import net.sf.jasperreports.export.SimpleExporterInput
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput
import net.sf.jasperreports.export.SimplePdfReportConfiguration
import net.sf.jasperreports.export.SimpleXlsReportConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pt.tiagocarvalho.dbexplorerui.conf.ConfigurationProperties
import pt.tiagocarvalho.dbexplorerui.exceptions.FileIsLockedException
import pt.tiagocarvalho.dbexplorerui.model.ReportInput
import pt.tiagocarvalho.dbexplorerui.utils.convertToMonth
import java.io.File
import java.io.FileOutputStream
import java.time.LocalDate

@Service
class JasperService {

    @Autowired
    lateinit var configurationProperties: ConfigurationProperties

    fun createPdfReport(reportInput: ReportInput): String {
        val stream = this.javaClass.getResourceAsStream("/report.jrxml")
        val report = JasperCompileManager.compileReport(stream)
        val parameters = mutableMapOf<String, Any>()
        val reportDate = createReportDate()

        val reportName = "me-${reportInput.code}"
        val fileName = "${configurationProperties.report.path}$reportName.pdf"
        val isLocked = isFileLocked(fileName)
        if (isLocked) throw FileIsLockedException()

        parameters["MONTH"] = reportInput.month
        parameters["YEAR"] = reportInput.year
        parameters["COMPANY_NAME"] = reportInput.companyName
        parameters["COMPANY_VAT"] = reportInput.companyVat
        parameters["REPORT_DATE"] = reportDate
        parameters["DATA_SOURCE_TABLE"] = JRBeanCollectionDataSource(reportInput.jasperRows)
        parameters["DATA_SOURCE_CONDITIONS"] = JRBeanCollectionDataSource(reportInput.conditions)

        val print = JasperFillManager.fillReport(report, parameters, JREmptyDataSource())

        val reportConfig = SimplePdfReportConfiguration()
            .apply {
                isSizePageToContent = true
                isForceLineBreakPolicy = false
            }

        JRPdfExporter()
            .apply {
                setExporterInput(SimpleExporterInput(print))
                exporterOutput = SimpleOutputStreamExporterOutput(fileName)
                setConfiguration(reportConfig)
                exportReport()
            }

        if (configurationProperties.report.pathsecondary.isNotBlank()) {
            val pcName: String = findPcName()
            val secondaryFileName = "${configurationProperties.report.pathsecondary}${pcName}\\$reportName.pdf"
            JRPdfExporter()
                .apply {
                    setExporterInput(SimpleExporterInput(print))
                    exporterOutput = SimpleOutputStreamExporterOutput(secondaryFileName)
                    setConfiguration(reportConfig)
                    exportReport()
                }
        }

        return reportName
    }

    private fun findPcName(): String {
        val env = System.getenv()
        val computerName = when {
            env.containsKey("COMPUTERNAME") -> env["COMPUTERNAME"]!!
            env.containsKey("USERDOMAIN") -> env["USERDOMAIN"]!!
            env.containsKey("USERDOMAIN_ROAMINGPROFILE") -> env["USERDOMAIN_ROAMINGPROFILE"]!!
            else -> throw IllegalArgumentException("Erro ao decobrir pasta de destino")
        }
        return computerName.takeLast(5)
    }

    fun createXlsReport(reportInput: ReportInput): String {
        val stream = this.javaClass.getResourceAsStream("/report.jrxml")
        val report = JasperCompileManager.compileReport(stream)
        val parameters = mutableMapOf<String, Any>()
        val reportDate = createReportDate()

        val reportName = "me-${reportInput.code}"
        val fileName = "${configurationProperties.report.path}$reportName.xls"
        val isLocked = isFileLocked(fileName)
        if (isLocked) throw FileIsLockedException()

        parameters["MONTH"] = reportInput.month
        parameters["YEAR"] = reportInput.year
        parameters["COMPANY_NAME"] = reportInput.companyName
        parameters["COMPANY_VAT"] = reportInput.companyVat
        parameters["REPORT_DATE"] = reportDate
        parameters["DATA_SOURCE_TABLE"] = JRBeanCollectionDataSource(reportInput.jasperRows)
        parameters["DATA_SOURCE_CONDITIONS"] = JRBeanCollectionDataSource(reportInput.conditions)

        val print = JasperFillManager.fillReport(report, parameters, JREmptyDataSource())

        val reportConfig = SimpleXlsReportConfiguration()
            .apply {
                isDetectCellType = true
                isOnePagePerSheet = false
                isWhitePageBackground = true
                isRemoveEmptySpaceBetweenRows = false
                isAutoFitPageHeight = true
                isRemoveEmptySpaceBetweenColumns = false
            }

        JRXlsExporter()
            .apply {
                setExporterInput(SimpleExporterInput(print))
                exporterOutput = SimpleOutputStreamExporterOutput(fileName)
                setConfiguration(reportConfig)
                exportReport()
            }

        return reportName
    }

    private fun isFileLocked(fileName: String): Boolean {
        val file = File(fileName)
        var fos: FileOutputStream? = null
        return try {
            fos = FileOutputStream(file)
            false
        } catch (e: Exception) {
            true
        } finally {
            fos?.close()
        }
    }

    private fun createReportDate(): String {
        val now = LocalDate.now()
        val day = now.dayOfMonth
        val month = now.monthValue
        val monthDisplay = month.convertToMonth()
        val year = now.year
        return "${day}-${monthDisplay}-${year}"
    }
}