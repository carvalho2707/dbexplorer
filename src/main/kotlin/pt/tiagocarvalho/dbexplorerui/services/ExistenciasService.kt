package pt.tiagocarvalho.dbexplorerui.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import pt.tiagocarvalho.dbexplorerui.entities.Controlo
import pt.tiagocarvalho.dbexplorerui.model.SaveExistencias
import pt.tiagocarvalho.dbexplorerui.repository.ControloRepository
import pt.tiagocarvalho.dbexplorerui.utils.toBigDecimalOrZero

@Service
class ExistenciasService {

    @Autowired
    lateinit var controloRepository: ControloRepository

    fun loadExistencias(clientNumber: String): List<String> {
        val controlo = controloRepository.findByIdOrNull(clientNumber)
        return mapControloToExistencias(controlo)
    }

    fun saveExistencias(saveExistencias: SaveExistencias) {
        var controlo = controloRepository.findByIdOrNull(saveExistencias.clientNumber)

        if (controlo == null) {
            controlo = Controlo(saveExistencias.clientNumber)
        }

        controlo.jan = saveExistencias.jan.toBigDecimalOrZero()
        controlo.fev = saveExistencias.fev.toBigDecimalOrZero()
        controlo.mar = saveExistencias.mar.toBigDecimalOrZero()
        controlo.apr = saveExistencias.apr.toBigDecimalOrZero()
        controlo.may = saveExistencias.may.toBigDecimalOrZero()
        controlo.jun = saveExistencias.jun.toBigDecimalOrZero()
        controlo.jul = saveExistencias.jul.toBigDecimalOrZero()
        controlo.aug = saveExistencias.aug.toBigDecimalOrZero()
        controlo.sep = saveExistencias.sep.toBigDecimalOrZero()
        controlo.oct = saveExistencias.oct.toBigDecimalOrZero()
        controlo.nov = saveExistencias.nov.toBigDecimalOrZero()
        controlo.dec = saveExistencias.dec.toBigDecimalOrZero()

        controloRepository.save(controlo)
    }

    private fun mapControloToExistencias(controlo: Controlo?): List<String> {
        return controlo?.let {
            listOf(
                it.jan.toPlainString(),
                it.fev.toPlainString(),
                it.mar.toPlainString(),
                it.apr.toPlainString(),
                it.may.toPlainString(),
                it.jun.toPlainString(),
                it.jul.toPlainString(),
                it.aug.toPlainString(),
                it.sep.toPlainString(),
                it.oct.toPlainString(),
                it.nov.toPlainString(),
                it.dec.toPlainString()
            )
        } ?: run {
            List(12) { "0.0" }
        }
    }
}