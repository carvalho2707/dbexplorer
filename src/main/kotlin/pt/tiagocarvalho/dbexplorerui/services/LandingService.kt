package pt.tiagocarvalho.dbexplorerui.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import pt.tiagocarvalho.dbexplorerui.model.ClientInfo
import pt.tiagocarvalho.dbexplorerui.model.ConditionRow
import pt.tiagocarvalho.dbexplorerui.model.JasperRow
import pt.tiagocarvalho.dbexplorerui.model.MonthRecord
import pt.tiagocarvalho.dbexplorerui.model.Mov
import pt.tiagocarvalho.dbexplorerui.model.ReportInput
import pt.tiagocarvalho.dbexplorerui.model.ReportType
import pt.tiagocarvalho.dbexplorerui.model.Results
import pt.tiagocarvalho.dbexplorerui.model.SearchVO
import pt.tiagocarvalho.dbexplorerui.model.SendReportVO
import pt.tiagocarvalho.dbexplorerui.model.TableRowInfo
import pt.tiagocarvalho.dbexplorerui.repository.CompanyRepository
import pt.tiagocarvalho.dbexplorerui.repository.ControloRepository
import pt.tiagocarvalho.dbexplorerui.repository.MovCtbRepository
import pt.tiagocarvalho.dbexplorerui.utils.PrepareUtils
import pt.tiagocarvalho.dbexplorerui.utils.ProcessingUtils
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_AMORTIZACOES
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_CONTA71
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_CONTA72_A
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_CONTA72_B
import pt.tiagocarvalho.dbexplorerui.utils.convertToMonth
import pt.tiagocarvalho.dbexplorerui.utils.sumByBigDecimal
import pt.tiagocarvalho.dbexplorerui.utils.toStringFormatted
import pt.tiagocarvalho.dbexplorerui.utils.useLines
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

@Service
class LandingService {

    @Autowired
    lateinit var movCtbRepository: MovCtbRepository

    @Autowired
    lateinit var companyRepository: CompanyRepository

    @Autowired
    lateinit var jasperService: JasperService

    @Autowired
    lateinit var controloRepository: ControloRepository

    @Value("classpath:companies.csv")
    lateinit var resourceFile: Resource

    private var existencias = mutableMapOf<Int, BigDecimal>()
    private var existenciasIniciais = mutableMapOf<Int, BigDecimal>()
    private var existenciasFinais = mutableMapOf<Int, BigDecimal>()
    private var cmvmcValues = mutableMapOf<Int, BigDecimal>()
    private var totaisNeg = mutableMapOf<Int, BigDecimal>()
    private var totaisPos = mutableMapOf<Int, BigDecimal>()
    private var totais = mutableMapOf<Int, BigDecimal>()
    private var monthlyRecords = mutableMapOf<Int, MonthRecord>()
    private var cachedResults = emptyList<TableRowInfo>()

    fun search(searchVO: SearchVO): Results {
        monthlyRecords = ProcessingUtils.createMonthlyRecords(searchVO.mes)
        existencias = prepareExistencias(searchVO.clientNumber)
        val records = movCtbRepository.getMovCtb(searchVO)
        applyFormulas(records, searchVO)

        val preparedResults = prepareResults()
        cachedResults = preparedResults

        clearValues()
        return Results(preparedResults)
    }

    fun saveReport(sendReportVO: SendReportVO, reportType: ReportType): String {
        val rows = convertToJasperRow(cachedResults)
        val conditions = mapConditions(sendReportVO, rows)
        val nameVat = companyRepository.getNameAndVat(sendReportVO.clientNumber, sendReportVO.ano)
        val reportInput = ReportInput(
            rows,
            conditions,
            sendReportVO.mes.convertToMonth(),
            sendReportVO.ano,
            nameVat.first,
            nameVat.second,
            sendReportVO.clientNumber
        )
        return when (reportType) {
            ReportType.XLSX -> jasperService.createXlsReport(reportInput)
            ReportType.PDF -> jasperService.createPdfReport(reportInput)
        }
    }

    private fun convertToJasperRow(cachedResults: List<TableRowInfo>): List<JasperRow> {
        return cachedResults.map {
            JasperRow(
                it.ct.get(),
                it.designacao.get(),
                it.jan.get(),
                it.feb.get(),
                it.mar.get(),
                it.apr.get(),
                it.may.get(),
                it.jun.get(),
                it.jul.get(),
                it.aug.get(),
                it.sep.get(),
                it.oct.get(),
                it.nov.get(),
                it.dec.get(),
                it.tot.get(),
                it.designacao.get() == "Existências Iniciais",
                it.isTotal,
                it.hasBackground,
                it.removeTotalBackground
            )
        }
    }

    private fun mapConditions(sendReportVO: SendReportVO, rows: List<JasperRow>): List<ConditionRow> {
        val exists = rows.take(5).any { it.tot.isNotEmpty() }
        val results = mutableListOf<ConditionRow>()

        if (exists) {
            if (sendReportVO.margemComerConta71 > BigDecimal.ZERO) {
                val conditionRow = ConditionRow(LABEL_CONTA71, sendReportVO.margemComerConta71.toStringFormatted("%"))
                results.add(conditionRow)
            }
            if (sendReportVO.margemComerConta72 > BigDecimal.ZERO) {
                val conditionRow = ConditionRow(LABEL_CONTA72_A, sendReportVO.margemComerConta72.toStringFormatted("%"))
                results.add(conditionRow)
            }
            if (sendReportVO.margemConsumoConta72 > BigDecimal.ZERO) {
                val conditionRow =
                    ConditionRow(LABEL_CONTA72_B, sendReportVO.margemConsumoConta72.toStringFormatted("%"))
                results.add(conditionRow)
            }
            if (sendReportVO.amortizacoes > BigDecimal.ZERO) {
                val conditionRow = ConditionRow(LABEL_AMORTIZACOES, sendReportVO.amortizacoes.toStringFormatted(2))
                results.add(conditionRow)
            }
        }

        return results
    }

    private fun prepareResults(): List<TableRowInfo> {
        val results = mutableListOf<TableRowInfo>()

        results.add(PrepareUtils.createCt1(existenciasIniciais))
        results.add(PrepareUtils.createEmptyLine())
        results.add(PrepareUtils.createCt31(monthlyRecords))
        results.add(PrepareUtils.createCt2(existenciasFinais))
        results.add(PrepareUtils.createCt3(cmvmcValues))
        results.add(PrepareUtils.createEmptyLine())
        results.add(PrepareUtils.createCt61(monthlyRecords))
        results.add(PrepareUtils.createCt62(monthlyRecords))
        results.add(PrepareUtils.createCt63(monthlyRecords))
        results.add(PrepareUtils.createCt27(monthlyRecords))
        results.add(PrepareUtils.createCt64(monthlyRecords))
        results.add(PrepareUtils.createCt65(monthlyRecords))
        results.add(PrepareUtils.createCt66(monthlyRecords))
        results.add(PrepareUtils.createCt67(monthlyRecords))
        results.add(PrepareUtils.createCt68(monthlyRecords))
        results.add(PrepareUtils.createCt69(monthlyRecords))
        results.add(PrepareUtils.createCtTotalIntermediate(totaisNeg))
        results.add(PrepareUtils.createEmptyLine())
        results.add(PrepareUtils.createCt71(monthlyRecords))
        results.add(PrepareUtils.createCt72(monthlyRecords))
        results.add(PrepareUtils.createCt73(monthlyRecords))
        results.add(PrepareUtils.createCt74(monthlyRecords))
        results.add(PrepareUtils.createCt75(monthlyRecords))
        results.add(PrepareUtils.createCt76(monthlyRecords))
        results.add(PrepareUtils.createCt77(monthlyRecords))
        results.add(PrepareUtils.createCt78(monthlyRecords))
        results.add(PrepareUtils.createCt79(monthlyRecords))
        results.add(PrepareUtils.createCtTotalIntermediate(totaisPos))
        results.add(PrepareUtils.createEmptyLine(true))
        results.add(PrepareUtils.createCtTotal(totais))


        return results
    }

    private fun clearValues() {
        existencias = mutableMapOf()
        existenciasIniciais = mutableMapOf()
        existenciasFinais = mutableMapOf()
        cmvmcValues = mutableMapOf()
        totaisNeg = mutableMapOf()
        totaisPos = mutableMapOf()
        totais = mutableMapOf()
        monthlyRecords = mutableMapOf()
    }

    private fun applyFormulas(records: List<Mov>, searchVO: SearchVO) {
        processExistenciaInicialAnoAnterior(records)
        processResults(records, searchVO.mes, searchVO.amortizacoes)
        processByMonthRecords(records, searchVO)
        process61(searchVO.mes)
        processTotal(searchVO.mes)
        processMonth13()
    }

    fun processByMonthRecords(records: List<Mov>, searchVO: SearchVO) {
        for (i in 1..searchVO.mes) {
            processExistenciasIniciais(i)
            //if existencias for the current month > 0 calculate first existencias finais then cmvmc else inverse
            if (existencias[1]!! > BigDecimal.ZERO) {
                processExistencias(i)
                processCmvmc(i, searchVO.margemComerConta71, searchVO.margemComerConta72, searchVO.margemConsumoConta72)
            } else {
                processCmvmc(i, searchVO.margemComerConta71, searchVO.margemComerConta72, searchVO.margemConsumoConta72)
                processExistencias(i)
            }
        }
    }

    private fun processResults(records: List<Mov>, months: Int, amort: BigDecimal) {
        records.forEach {
            when (it.id) {
                "31", "62", "63", "65", "66", "67", "68", "69" -> processGenericExpense(it, it.id)
                "71", "72", "73", "74", "75", "76", "77", "78", "79" -> processGenericInvoice(it, it.id)
                "38" -> processPositive(it, it.id)
                "27" -> return@forEach //27 is handled afterwards since
                "64" -> return@forEach //64 is handled afterwards since
                "61" -> return@forEach //61 is handled afterwards since
            }
        }

        process27(records, months)
        process64(amort, months)
    }

    private fun processGenericExpense(mov: Mov, id: String) {
        if (mov.mes >= 1) {
            val value = when (mov.cDoc) {
                "D" -> mov.valor
                "C" -> mov.valor.negate()
                else -> BigDecimal.ZERO
            }
            monthlyRecords[mov.mes]!!.update(id, value)
        }
    }

    private fun processGenericInvoice(mov: Mov, id: String) {
        if (mov.mes >= 1) {
            val value = when (mov.cDoc) {
                "C" -> mov.valor
                "D" -> mov.valor.negate()
                else -> BigDecimal.ZERO
            }
            monthlyRecords[mov.mes]!!.update(id, value)
        }
    }

    private fun processPositive(mov: Mov, id: String) {
        if (mov.mes >= 1) {
            monthlyRecords[mov.mes]!!.update(id, mov.valor)
        }
    }

    private fun process27(records: List<Mov>, months: Int) {
        val total = records.filter { it.id == "27" && it.mes == 0 }
            .map {
                if (it.cDoc == "C") it.valor
                else it.valor.negate()
            }
            .sumByBigDecimal { it }
        val temp = total.divide(BigDecimal("2"), 4, RoundingMode.HALF_UP)
        val amount = temp.multiply(BigDecimal(3), MathContext(8)).divide(BigDecimal(12), RoundingMode.HALF_UP)

        for (i in 1..months) {
            monthlyRecords[i]!!.update("27", amount)
        }
    }

    private fun process64(amort: BigDecimal, months: Int) {
        val amount = amort.divide(BigDecimal("12"), 4, RoundingMode.HALF_UP)
        for (i in 1..months) {
            monthlyRecords[i]!!.update("64", amount)
        }
    }

    private fun process61(months: Int) {
        for (i in 1..months) {
            monthlyRecords[i]!!.update("61", cmvmcValues[i]!!)
        }
    }

    private fun processCmvmc(month: Int, mComer71: BigDecimal, mComer72: BigDecimal, mCons72: BigDecimal) {
        val cmvmc = if (existencias[1]!! > BigDecimal.ZERO) {
            existenciasIniciais[month]!!.add(monthlyRecords[month]!!.categories["31"]!!)
                .subtract(existenciasFinais[month]!!)
        } else {
            val tempMComer71 = BigDecimal.ONE.add(mComer71)
            val tempMComer72 = BigDecimal.ONE.add(mComer72)
            val first = monthlyRecords[month]!!.categories["71"]!!.divide(tempMComer71, 8, RoundingMode.HALF_UP)
            val second: BigDecimal = when {
                mCons72 > BigDecimal.ZERO -> monthlyRecords[month]!!.categories["72"]!!.multiply(mCons72)
                mComer72 > BigDecimal.ZERO -> monthlyRecords[month]!!.categories["72"]!!.divide(
                    tempMComer72,
                    8,
                    RoundingMode.HALF_UP
                )
                else -> BigDecimal.ZERO
            }
            first.add(second)
        }

        cmvmcValues[month] = cmvmc
    }

    private fun processExistenciasIniciais(month: Int) {
        if (month == 1) {
            return
        } else {
            if (existencias[1]!! > BigDecimal.ZERO) {
                existenciasIniciais[month] = existencias[month - 1]!!
            } else {
                existenciasIniciais[month] = existenciasFinais[month - 1]!!
            }
        }
    }

    private fun processExistencias(month: Int) {
        if (month == 1) {
            if (existencias[1]!! > BigDecimal.ZERO) {
                existenciasFinais[month] = existencias[1]!!
            } else {
                existenciasFinais[month] = existenciasIniciais[month]!!
                    .add(monthlyRecords[month]!!.categories["31"]!!)
                    .subtract(cmvmcValues[month])
                    .subtract(monthlyRecords[month]!!.categories["38"]!!)
            }
        } else {
            if (existencias[1]!! > BigDecimal.ZERO) {
                existenciasFinais[month] = existencias[month]!!
            } else {
                existenciasFinais[month] = existenciasIniciais[month]!!
                    .add(monthlyRecords[month]!!.categories["31"]!!)
                    .subtract(cmvmcValues[month])
                    .subtract(monthlyRecords[month]!!.categories["38"]!!)
            }
        }
    }

    private fun processTotal(months: Int) {
        for (i in 1..months) {
            val totalNeg = monthlyRecords[i]!!.categories["61"]!!
                .add(monthlyRecords[i]!!.categories["62"]!!)
                .add(monthlyRecords[i]!!.categories["63"]!!)
                .add(monthlyRecords[i]!!.categories["27"]!!)
                .add(monthlyRecords[i]!!.categories["64"]!!)
                .add(monthlyRecords[i]!!.categories["65"]!!)
                .add(monthlyRecords[i]!!.categories["66"]!!)
                .add(monthlyRecords[i]!!.categories["67"]!!)
                .add(monthlyRecords[i]!!.categories["68"]!!)
                .add(monthlyRecords[i]!!.categories["69"]!!)
            totaisNeg[i] = totalNeg

            val totalPos = monthlyRecords[i]!!.categories["71"]!!
                .add(monthlyRecords[i]!!.categories["72"]!!)
                .add(monthlyRecords[i]!!.categories["73"]!!)
                .add(monthlyRecords[i]!!.categories["74"]!!)
                .add(monthlyRecords[i]!!.categories["75"]!!)
                .add(monthlyRecords[i]!!.categories["76"]!!)
                .add(monthlyRecords[i]!!.categories["77"]!!)
                .add(monthlyRecords[i]!!.categories["78"]!!)
                .add(monthlyRecords[i]!!.categories["79"]!!)
            totaisPos[i] = totalPos

            val total = totalPos.subtract(totalNeg)
            totais[i] = total
        }
    }

    private fun processMonth13() {
        val totalCmvmc = cmvmcValues.map { it.value }.sumByBigDecimal { it }
        cmvmcValues[13] = totalCmvmc

        val total31 = monthlyRecords.map { it.value.categories["31"]!! }.sumByBigDecimal { it }
        val total61 = monthlyRecords.map { it.value.categories["61"]!! }.sumByBigDecimal { it }
        val total62 = monthlyRecords.map { it.value.categories["62"]!! }.sumByBigDecimal { it }
        val total63 = monthlyRecords.map { it.value.categories["63"]!! }.sumByBigDecimal { it }
        val total27 = monthlyRecords.map { it.value.categories["27"]!! }.sumByBigDecimal { it }
        val total64 = monthlyRecords.map { it.value.categories["64"]!! }.sumByBigDecimal { it }
        val total65 = monthlyRecords.map { it.value.categories["65"]!! }.sumByBigDecimal { it }
        val total66 = monthlyRecords.map { it.value.categories["66"]!! }.sumByBigDecimal { it }
        val total67 = monthlyRecords.map { it.value.categories["67"]!! }.sumByBigDecimal { it }
        val total68 = monthlyRecords.map { it.value.categories["68"]!! }.sumByBigDecimal { it }
        val total69 = monthlyRecords.map { it.value.categories["69"]!! }.sumByBigDecimal { it }
        val totalsNeg13 = totaisNeg.map { it.value }.sumByBigDecimal { it }

        val total71 = monthlyRecords.map { it.value.categories["71"]!! }.sumByBigDecimal { it }
        val total72 = monthlyRecords.map { it.value.categories["72"]!! }.sumByBigDecimal { it }
        val total73 = monthlyRecords.map { it.value.categories["73"]!! }.sumByBigDecimal { it }
        val total74 = monthlyRecords.map { it.value.categories["74"]!! }.sumByBigDecimal { it }
        val total75 = monthlyRecords.map { it.value.categories["75"]!! }.sumByBigDecimal { it }
        val total76 = monthlyRecords.map { it.value.categories["76"]!! }.sumByBigDecimal { it }
        val total77 = monthlyRecords.map { it.value.categories["77"]!! }.sumByBigDecimal { it }
        val total78 = monthlyRecords.map { it.value.categories["78"]!! }.sumByBigDecimal { it }
        val total79 = monthlyRecords.map { it.value.categories["79"]!! }.sumByBigDecimal { it }

        val totalsPos13 = totaisPos.map { it.value }.sumByBigDecimal { it }
        val totals13 = totalsPos13.subtract(totalsNeg13)


        monthlyRecords[13] = MonthRecord(ProcessingUtils.populateCategories())

        monthlyRecords[13].apply {
            //neg
            this!!.update("31", total31)
            this.update("61", total61)
            this.update("62", total62)
            this.update("63", total63)
            this.update("27", total27)
            this.update("64", total64)
            this.update("65", total65)
            this.update("66", total66)
            this.update("67", total67)
            this.update("68", total68)
            this.update("69", total69)

            //pos
            this.update("71", total71)
            this.update("72", total72)
            this.update("73", total73)
            this.update("74", total74)
            this.update("75", total75)
            this.update("76", total76)
            this.update("77", total77)
            this.update("78", total78)
            this.update("79", total79)
        }

        totaisNeg[13] = totalsNeg13
        totaisPos[13] = totalsPos13
        totais[13] = totals13
    }

    private fun processExistenciaInicialAnoAnterior(records: List<Mov>) {
        var result = BigDecimal.ZERO
        records
            .filter { it.mes == 0 }
            .forEach {
                if (it.id.startsWith("32") && it.cDoc == "D") {
                    result = result.add(it.valor)
                } else if (it.id.startsWith("32") && it.cDoc == "C") {
                    result = result.subtract(it.valor)
                } else if (it.id.startsWith("33") && it.cDoc == "D") {
                    result = result.add(it.valor)
                } else if (it.id.startsWith("33") && it.cDoc == "C") {
                    result = result.subtract(it.valor)
                }
            }
        existenciasIniciais[1] = result
    }

    private fun prepareExistencias(clientNumber: String): MutableMap<Int, BigDecimal> {
        val existencias = controloRepository.findByIdOrNull(clientNumber)
        return if (existencias == null) {
            ProcessingUtils.createEmptyExistencias()
        } else {
            val result = mutableMapOf<Int, BigDecimal>()
            result[1] = existencias.jan
            result[2] = existencias.fev
            result[3] = existencias.mar
            result[4] = existencias.apr
            result[5] = existencias.may
            result[6] = existencias.jun
            result[7] = existencias.jul
            result[8] = existencias.aug
            result[9] = existencias.sep
            result[10] = existencias.oct
            result[11] = existencias.nov
            result[12] = existencias.dec
            return result
        }
    }

    fun readClients(): List<ClientInfo> {
        return resourceFile.inputStream.useLines { linesSequence ->
            linesSequence
                .map { it.split(",") }
                .map { ClientInfo(it[0], it[1].replace("\"", "")) }
                .toList()
        }
    }
}