package pt.tiagocarvalho.dbexplorerui

import javafx.application.Application
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import pt.tiagocarvalho.dbexplorerui.conf.ConfigurationProperties

@EnableConfigurationProperties(ConfigurationProperties::class)
@SpringBootApplication
class DbexplorerUiApplication

fun main() {
    System.setProperty("java.awt.headless", "false")
    Application.launch(EnterApplication::class.java)
}
