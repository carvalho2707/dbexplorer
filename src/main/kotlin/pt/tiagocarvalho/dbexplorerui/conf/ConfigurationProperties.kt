package pt.tiagocarvalho.dbexplorerui.conf

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "dbexplorer")
class ConfigurationProperties {
    val db = Db()
    val report = Report()

    class Db {
        lateinit var url: String
        lateinit var username: String
        lateinit var password: String
    }

    class Report {
        lateinit var path: String
        lateinit var pathsecondary: String
    }
}