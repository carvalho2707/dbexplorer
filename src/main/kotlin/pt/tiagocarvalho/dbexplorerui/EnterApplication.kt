package pt.tiagocarvalho.dbexplorerui

import javafx.application.Application
import javafx.application.Platform
import javafx.stage.Stage
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.ApplicationEvent
import org.springframework.context.ConfigurableApplicationContext

class EnterApplication : Application() {

    private lateinit var applicationContext: ConfigurableApplicationContext

    override fun start(primaryStage: Stage) {
        applicationContext.publishEvent(StageReadyEvent(primaryStage))
    }

    override fun init() {
        applicationContext = SpringApplicationBuilder(DbexplorerUiApplication::class.java).run()
    }

    override fun stop() {
        applicationContext.stop()
        Platform.exit()
    }

    class StageReadyEvent(stage: Stage) : ApplicationEvent(stage) {

        fun getStage(): Any = getSource()
    }
}