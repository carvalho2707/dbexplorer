package pt.tiagocarvalho.dbexplorerui.utils

import pt.tiagocarvalho.dbexplorerui.model.MonthRecord
import java.math.BigDecimal

object ProcessingUtils {

    fun createMonthlyRecords(months: Int): MutableMap<Int, MonthRecord> {
        val result = mutableMapOf<Int, MonthRecord>()
        for (i in 1..months) {
            val categories = populateCategories()
            val monthRecord = MonthRecord(categories)
            result[i] = monthRecord
        }
        return result
    }

    fun createEmptyExistencias(): MutableMap<Int, BigDecimal> {
        val result = mutableMapOf<Int, BigDecimal>()
        result[1] = BigDecimal.ZERO
        result[2] = BigDecimal.ZERO
        result[3] = BigDecimal.ZERO
        result[4] = BigDecimal.ZERO
        result[5] = BigDecimal.ZERO
        result[6] = BigDecimal.ZERO
        result[7] = BigDecimal.ZERO
        result[8] = BigDecimal.ZERO
        result[9] = BigDecimal.ZERO
        result[10] = BigDecimal.ZERO
        result[11] = BigDecimal.ZERO
        result[12] = BigDecimal.ZERO
        return result
    }

    fun populateCategories(): MutableMap<String, BigDecimal> {
        val result = mutableMapOf<String, BigDecimal>()
        result["31"] = BigDecimal.ZERO
        result["61"] = BigDecimal.ZERO
        result["62"] = BigDecimal.ZERO
        result["63"] = BigDecimal.ZERO
        result["27"] = BigDecimal.ZERO
        result["64"] = BigDecimal.ZERO
        result["65"] = BigDecimal.ZERO
        result["66"] = BigDecimal.ZERO
        result["67"] = BigDecimal.ZERO
        result["68"] = BigDecimal.ZERO
        result["69"] = BigDecimal.ZERO
        result["71"] = BigDecimal.ZERO
        result["72"] = BigDecimal.ZERO
        result["73"] = BigDecimal.ZERO
        result["74"] = BigDecimal.ZERO
        result["75"] = BigDecimal.ZERO
        result["76"] = BigDecimal.ZERO
        result["77"] = BigDecimal.ZERO
        result["78"] = BigDecimal.ZERO
        result["79"] = BigDecimal.ZERO
        result["38"] = BigDecimal.ZERO
        return result
    }
}