package pt.tiagocarvalho.dbexplorerui.utils

import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn
import javafx.util.Callback
import pt.tiagocarvalho.dbexplorerui.model.TableRowInfo

class FormattedTableCellFactory<S, T> : Callback<TableColumn<S, T>?, TableCell<S, T>?> {
    override fun call(p: TableColumn<S, T>?): TableCell<S, T> {
        return object : TableCell<S, T>() {
            override fun updateItem(item: T, empty: Boolean) {
                // CSS Styles
                val totalIntermediateStyle = "totalIntermediate"
                var tableRowInfo: TableRowInfo? = null

                if (tableRow != null && tableRow.item != null) {
                    tableRowInfo = tableRow.item as TableRowInfo
                }

                //Remove all previously assigned CSS styles from the cell.
                styleClass.remove(totalIntermediateStyle)
                styleClass.remove("total")
                super.updateItem(item as T?, empty)

                //Set the CSS style on the cell and set the cell's text.
                if (tableRowInfo != null) {
                    if (tableRowInfo.hasBackground) {
                        styleClass.add("totalIntermediate")
                    }
                    if (tableRowInfo.isTotal) {
                        styleClass.add("total")
                    }

                    tableRow.selectedProperty().addListener { _, _, newValue ->
                        if (tableRowInfo.hasBackground) {
                            if (newValue) {
                                styleClass.remove("totalIntermediate")
                                styleClass.add("totalIntermediateMixed")
                            } else {
                                styleClass.remove("totalIntermediateMixed")
                                styleClass.add("totalIntermediate")
                            }
                        }
                    }
                }

                if (item != null) {
                    setText(item.toString())
                } else {
                    setText("")
                }
            }
        }
    }
}