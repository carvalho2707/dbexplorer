package pt.tiagocarvalho.dbexplorerui.utils

import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TextField
import pt.tiagocarvalho.dbexplorerui.utils.Properties.SCALE
import java.io.InputStream
import java.math.BigDecimal
import java.math.RoundingMode
import java.nio.charset.Charset
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

fun BigDecimal.toSimpleStringProperty(): SimpleStringProperty {
    val decimalFormatSymbols = DecimalFormatSymbols()
    decimalFormatSymbols.decimalSeparator = '.'
    decimalFormatSymbols.groupingSeparator = ' '
    val decimalFormat = DecimalFormat("#,##0", decimalFormatSymbols)
    return if (this.compareTo(BigDecimal.ZERO) == 0) {
        SimpleStringProperty("-")
    } else {
        SimpleStringProperty(decimalFormat.format(this.setScale(SCALE, RoundingMode.HALF_UP)))
    }
}

fun BigDecimal.toStringFormatted(symbol: String = "€"): String {
    val decimalFormatSymbols = DecimalFormatSymbols()
    decimalFormatSymbols.decimalSeparator = '.'
    decimalFormatSymbols.groupingSeparator = ' '
    val decimalFormat = DecimalFormat("#,##0", decimalFormatSymbols)
    return decimalFormat.format(this.setScale(SCALE, RoundingMode.HALF_UP)) + " $symbol"
}

fun BigDecimal.toStringFormatted(scale: Int): String {
    val decimalFormatSymbols = DecimalFormatSymbols()
    decimalFormatSymbols.decimalSeparator = '.'
    decimalFormatSymbols.groupingSeparator = ' '
    val decimalFormat = DecimalFormat("#,###.00", decimalFormatSymbols)
    return decimalFormat.format(this.setScale(scale, RoundingMode.HALF_UP)) + " €"
}

inline fun <T> Iterable<T>.sumByBigDecimal(selector: (T) -> BigDecimal): BigDecimal {
    var sum: BigDecimal = BigDecimal.ZERO
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

fun Int.convertToMonth(): String = when (this) {
    1 -> Properties.MONTH_JANUARY
    2 -> Properties.MONTH_FEBRUARY
    3 -> Properties.MONTH_MARCH
    4 -> Properties.MONTH_APRIL
    5 -> Properties.MONTH_MAY
    6 -> Properties.MONTH_JUNE
    7 -> Properties.MONTH_JULY
    8 -> Properties.MONTH_AUGUST
    9 -> Properties.MONTH_SEPTEMBER
    10 -> Properties.MONTH_OCTOBER
    11 -> Properties.MONTH_NOVEMBER
    12 -> Properties.MONTH_DECEMBER
    else -> ""
}

inline fun <T> InputStream.useLines(charset: Charset = Charsets.UTF_8, block: (Sequence<String>) -> T): T =
    bufferedReader(charset).use { block(it.lineSequence()) }

fun String.toBigDecimalOrZero(): BigDecimal = this.toBigDecimalOrNull() ?: BigDecimal.ZERO

fun TextField.isZero(): Boolean {
    val plainText = this.text.replace("[^0-9]".toRegex(), "")
    val number = plainText.toIntOrNull()
    return number == null || number == 0
}

fun TextField.isNotZero(): Boolean {
    return !this.isZero()
}
