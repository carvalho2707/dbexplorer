package pt.tiagocarvalho.dbexplorerui.utils

import javafx.scene.control.TextFormatter
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.Locale
import java.util.function.UnaryOperator

object FormatterUtils {

    fun intFormatter(max: Int): TextFormatter<Int> {
        return TextFormatter(UnaryOperator { c ->
            // Deletion should always be possible.
            if (c.isDeleted) {
                return@UnaryOperator c
            }

            val txt = c.controlNewText

            // There shouldn't be leading zeros.
            if (txt.matches(("0\\d+").toRegex())) {
                return@UnaryOperator null
            }

            try {
                val n = txt.toInt()
                if (n in 1 until max + 1) return@UnaryOperator c
                return@UnaryOperator null
            } catch (e: Exception) {
                return@UnaryOperator null
            }
        })
    }

    fun getCurrencyNumberFormat(): NumberFormat {
        val numberFormat = NumberFormat.getCurrencyInstance(Locale("pt", "PT"))
        return setupNumberFormat(numberFormat)
    }

    fun getPercentageNumberFormat(): NumberFormat {
        val numberFormat = NumberFormat.getPercentInstance(Locale("pt", "PT"))
        return setupNumberFormat(numberFormat)
    }

    private fun setupNumberFormat(numberFormat: NumberFormat): NumberFormat {
        numberFormat.minimumFractionDigits = 2
        numberFormat.maximumFractionDigits = 2
        val decimalFormat = numberFormat as DecimalFormat
        val decimalFormatSymbols = decimalFormat.decimalFormatSymbols
        decimalFormatSymbols.decimalSeparator = '.'
        decimalFormatSymbols.monetaryDecimalSeparator = '.'
        decimalFormatSymbols.groupingSeparator = ','
        decimalFormat.decimalFormatSymbols = decimalFormatSymbols
        return decimalFormat
    }
}