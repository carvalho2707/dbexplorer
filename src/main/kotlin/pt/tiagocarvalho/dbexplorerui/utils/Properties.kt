package pt.tiagocarvalho.dbexplorerui.utils

object Properties {

    const val SCALE = 0

    const val CLIENT_REQUIRED = "O número de cliente é obrigatório"
    const val CLIENT_NOT_FOUND = "Não foi encontrado nenhum cliente com o nome %s"

    const val MONTH_JANUARY = "Janeiro"
    const val MONTH_FEBRUARY = "Fevereiro"
    const val MONTH_MARCH = "Março"
    const val MONTH_APRIL = "Abril"
    const val MONTH_MAY = "Maio"
    const val MONTH_JUNE = "Junho"
    const val MONTH_JULY = "Julho"
    const val MONTH_AUGUST = "Agosto"
    const val MONTH_SEPTEMBER = "Setembro"
    const val MONTH_OCTOBER = "Outubro"
    const val MONTH_NOVEMBER = "Novembro"
    const val MONTH_DECEMBER = "Dezembro"

    const val LABEL_EXISTENCIAS_INICIAIS = "Existências Iniciais"
    const val LABEL_EXISTENCIAS_FINAIS = "Existências Finais"
    const val LABEL_CMVMC = "CMVMC"
    const val LABEL_31 = "Compras"
    const val LABEL_61 = "Custo Mercadorias Vendidas"
    const val LABEL_62 = "Fornecimento Serviços Externos"
    const val LABEL_63 = "Gastos c/ Pessoal"
    const val LABEL_PROVISAO = "Provisão c/ Pessoal"
    const val LABEL_64 = "Gastos de Depreciação e Amortização"
    const val LABEL_65 = "Perdas por Imparidade"
    const val LABEL_66 = "Perdas por Reduções de Justo Valor"
    const val LABEL_67 = "Provisões do Periodo"
    const val LABEL_68 = "Outros Gastos e Perdas"
    const val LABEL_69 = "Gastos e Perdas de Financiamento"
    const val LABEL_71 = "Vendas"
    const val LABEL_72 = "Prestação Serviços"
    const val LABEL_73 = "Variações nos Inventários da Produção"
    const val LABEL_74 = "Trabalhos Própria Empresa"
    const val LABEL_75 = "Subsídios Exploração"
    const val LABEL_76 = "Reversões"
    const val LABEL_77 = "Ganhos por Aumentos de Justo Valor"
    const val LABEL_78 = "Outros Rendimentos e Ganhos"
    const val LABEL_79 = "Juros, Dividendos e Outros Rendimentos Similares"
    const val LABEL_TOTAL = "Total"
    const val LABEL_RESULTADOS = "Resultados de Exploração"

    const val LABEL_CONTA71 = "Conta 71 (Margem Comercialização)"
    const val LABEL_CONTA72_A = "Conta 72 (Margem Comercialização)"
    const val LABEL_CONTA72_B = "Conta 72 (Margem Consumo)"
    const val LABEL_AMORTIZACOES = "Amortizações"
}