package pt.tiagocarvalho.dbexplorerui.utils

import pt.tiagocarvalho.dbexplorerui.model.MonthRecord
import pt.tiagocarvalho.dbexplorerui.model.TableRowInfo
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_31
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_61
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_62
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_63
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_64
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_65
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_66
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_67
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_68
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_69
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_71
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_72
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_73
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_74
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_75
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_76
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_77
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_78
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_79
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_CMVMC
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_EXISTENCIAS_FINAIS
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_EXISTENCIAS_INICIAIS
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_PROVISAO
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_RESULTADOS
import pt.tiagocarvalho.dbexplorerui.utils.Properties.LABEL_TOTAL
import java.math.BigDecimal

object PrepareUtils {

    fun createEmptyLine(removeTotalBackground: Boolean = false): TableRowInfo {
        return TableRowInfo(removeTotalBackground)
    }

    fun createCt1(existenciasIniciais: Map<Int, BigDecimal>): TableRowInfo {
        return TableRowInfo(
            "",
            LABEL_EXISTENCIAS_INICIAIS,
            false,
            existenciasIniciais.getOrElse(1) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(2) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(3) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(4) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(5) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(6) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(7) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(8) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(9) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(10) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(11) { BigDecimal.ZERO },
            existenciasIniciais.getOrElse(12) { BigDecimal.ZERO },
            BigDecimal.ZERO,
            isTotal = false,
            removeTotalBackground = false
        )
    }

    fun createCt2(existenciasFinais: Map<Int, BigDecimal>): TableRowInfo {
        return TableRowInfo(
            "",
            LABEL_EXISTENCIAS_FINAIS,
            false,
            existenciasFinais.getOrElse(1) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(2) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(3) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(4) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(5) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(6) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(7) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(8) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(9) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(10) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(11) { BigDecimal.ZERO },
            existenciasFinais.getOrElse(12) { BigDecimal.ZERO },
            BigDecimal.ZERO,
            isTotal = false,
            removeTotalBackground = false
        )
    }

    fun createCt3(cmvmValues: Map<Int, BigDecimal>): TableRowInfo {
        return TableRowInfo(
            "",
            LABEL_CMVMC,
            false,
            cmvmValues.getOrElse(1) { BigDecimal.ZERO },
            cmvmValues.getOrElse(2) { BigDecimal.ZERO },
            cmvmValues.getOrElse(3) { BigDecimal.ZERO },
            cmvmValues.getOrElse(4) { BigDecimal.ZERO },
            cmvmValues.getOrElse(5) { BigDecimal.ZERO },
            cmvmValues.getOrElse(6) { BigDecimal.ZERO },
            cmvmValues.getOrElse(7) { BigDecimal.ZERO },
            cmvmValues.getOrElse(8) { BigDecimal.ZERO },
            cmvmValues.getOrElse(9) { BigDecimal.ZERO },
            cmvmValues.getOrElse(10) { BigDecimal.ZERO },
            cmvmValues.getOrElse(11) { BigDecimal.ZERO },
            cmvmValues.getOrElse(12) { BigDecimal.ZERO },
            cmvmValues.getOrElse(13) { BigDecimal.ZERO },
            isTotal = false,
            removeTotalBackground = false
        )
    }

    fun createCt31(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("31", LABEL_31, monthlyRecords)
    }

    fun createCt61(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("61", LABEL_61, monthlyRecords)
    }

    fun createCt62(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("62", LABEL_62, monthlyRecords)
    }

    fun createCt63(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("63", LABEL_63, monthlyRecords)
    }

    fun createCt27(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return TableRowInfo(
            "",
            LABEL_PROVISAO,
            false,
            getCategory("27", monthlyRecords[1]),
            getCategory("27", monthlyRecords[2]),
            getCategory("27", monthlyRecords[3]),
            getCategory("27", monthlyRecords[4]),
            getCategory("27", monthlyRecords[5]),
            getCategory("27", monthlyRecords[6]),
            getCategory("27", monthlyRecords[7]),
            getCategory("27", monthlyRecords[8]),
            getCategory("27", monthlyRecords[9]),
            getCategory("27", monthlyRecords[10]),
            getCategory("27", monthlyRecords[11]),
            getCategory("27", monthlyRecords[12]),
            getCategory("27", monthlyRecords[13]),
            isTotal = false,
            removeTotalBackground = false
        )
    }

    fun createCt64(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("64", LABEL_64, monthlyRecords)
    }

    fun createCt65(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("65", LABEL_65, monthlyRecords)
    }

    fun createCt66(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("66", LABEL_66, monthlyRecords)
    }

    fun createCt67(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("67", LABEL_67, monthlyRecords)
    }

    fun createCt68(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("68", LABEL_68, monthlyRecords)
    }

    fun createCt69(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("69", LABEL_69, monthlyRecords)
    }

    fun createCt71(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("71", LABEL_71, monthlyRecords)
    }

    fun createCt72(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("72", LABEL_72, monthlyRecords)
    }

    fun createCt73(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("73", LABEL_73, monthlyRecords)
    }

    fun createCt74(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("74", LABEL_74, monthlyRecords)
    }

    fun createCt75(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("75", LABEL_75, monthlyRecords)
    }

    fun createCt76(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("76", LABEL_76, monthlyRecords)
    }

    fun createCt77(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("77", LABEL_77, monthlyRecords)
    }

    fun createCt78(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("78", LABEL_78, monthlyRecords)
    }

    fun createCt79(monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return createGeneric("79", LABEL_79, monthlyRecords)
    }

    fun createCtTotalIntermediate(totals: Map<Int, BigDecimal>): TableRowInfo {
        return TableRowInfo(
            "",
            LABEL_TOTAL,
            true,
            totals.getOrElse(1) { BigDecimal.ZERO },
            totals.getOrElse(2) { BigDecimal.ZERO },
            totals.getOrElse(3) { BigDecimal.ZERO },
            totals.getOrElse(4) { BigDecimal.ZERO },
            totals.getOrElse(5) { BigDecimal.ZERO },
            totals.getOrElse(6) { BigDecimal.ZERO },
            totals.getOrElse(7) { BigDecimal.ZERO },
            totals.getOrElse(8) { BigDecimal.ZERO },
            totals.getOrElse(9) { BigDecimal.ZERO },
            totals.getOrElse(10) { BigDecimal.ZERO },
            totals.getOrElse(11) { BigDecimal.ZERO },
            totals.getOrElse(12) { BigDecimal.ZERO },
            totals.getOrElse(13) { BigDecimal.ZERO },
            isTotal = false,
            removeTotalBackground = false
        )
    }

    fun createCtTotal(totals: Map<Int, BigDecimal>): TableRowInfo {
        return TableRowInfo(
            "",
            LABEL_RESULTADOS,
            false,
            totals.getOrElse(1) { BigDecimal.ZERO },
            totals.getOrElse(2) { BigDecimal.ZERO },
            totals.getOrElse(3) { BigDecimal.ZERO },
            totals.getOrElse(4) { BigDecimal.ZERO },
            totals.getOrElse(5) { BigDecimal.ZERO },
            totals.getOrElse(6) { BigDecimal.ZERO },
            totals.getOrElse(7) { BigDecimal.ZERO },
            totals.getOrElse(8) { BigDecimal.ZERO },
            totals.getOrElse(9) { BigDecimal.ZERO },
            totals.getOrElse(10) { BigDecimal.ZERO },
            totals.getOrElse(11) { BigDecimal.ZERO },
            totals.getOrElse(12) { BigDecimal.ZERO },
            totals.getOrElse(13) { BigDecimal.ZERO },
            isTotal = true,
            removeTotalBackground = true
        )
    }

    private fun getCategory(id: String, monthRecord: MonthRecord?): BigDecimal {
        return monthRecord?.let {
            it.categories.getOrElse(id) { BigDecimal.ZERO }
        } ?: BigDecimal.ZERO
    }

    private fun createGeneric(id: String, des: String, monthlyRecords: MutableMap<Int, MonthRecord>): TableRowInfo {
        return TableRowInfo(
            id,
            des,
            false,
            getCategory(id, monthlyRecords[1]),
            getCategory(id, monthlyRecords[2]),
            getCategory(id, monthlyRecords[3]),
            getCategory(id, monthlyRecords[4]),
            getCategory(id, monthlyRecords[5]),
            getCategory(id, monthlyRecords[6]),
            getCategory(id, monthlyRecords[7]),
            getCategory(id, monthlyRecords[8]),
            getCategory(id, monthlyRecords[9]),
            getCategory(id, monthlyRecords[10]),
            getCategory(id, monthlyRecords[11]),
            getCategory(id, monthlyRecords[12]),
            getCategory(id, monthlyRecords[13]),
            isTotal = false,
            removeTotalBackground = false
        )
    }
}