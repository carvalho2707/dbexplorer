package pt.tiagocarvalho.dbexplorerui.controller

import javafx.fxml.FXML
import javafx.scene.control.TextField
import javafx.stage.Stage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pt.tiagocarvalho.dbexplorerui.model.SaveExistencias
import pt.tiagocarvalho.dbexplorerui.services.ExistenciasService

@Component
class ExistenciasController {

    @FXML
    lateinit var tfJan: TextField

    @FXML
    lateinit var tfFev: TextField

    @FXML
    lateinit var tfMar: TextField

    @FXML
    lateinit var tfApr: TextField

    @FXML
    lateinit var tfMay: TextField

    @FXML
    lateinit var tfJun: TextField

    @FXML
    lateinit var tfJul: TextField

    @FXML
    lateinit var tfAug: TextField

    @FXML
    lateinit var tfSep: TextField

    @FXML
    lateinit var tfOct: TextField

    @FXML
    lateinit var tfNov: TextField

    @FXML
    lateinit var tfDec: TextField

    @Autowired
    lateinit var existenciasService: ExistenciasService

    var clientNumber: String = ""

    fun setInfo(client: String, existenciasService: ExistenciasService) {
        clientNumber = client
        this.existenciasService = existenciasService
    }

    fun loadIntoView() {
        val existencias: List<String> = existenciasService.loadExistencias(clientNumber)
        tfJan.text = existencias[0]
        tfFev.text = existencias[1]
        tfMar.text = existencias[2]
        tfApr.text = existencias[3]
        tfMay.text = existencias[4]
        tfJun.text = existencias[5]
        tfJul.text = existencias[6]
        tfAug.text = existencias[7]
        tfSep.text = existencias[8]
        tfOct.text = existencias[9]
        tfNov.text = existencias[10]
        tfDec.text = existencias[11]
    }

    @FXML
    fun btnClear() {
        tfJan.text = "0.0"
        tfFev.text = "0.0"
        tfMar.text = "0.0"
        tfApr.text = "0.0"
        tfMay.text = "0.0"
        tfJun.text = "0.0"
        tfJul.text = "0.0"
        tfAug.text = "0.0"
        tfSep.text = "0.0"
        tfOct.text = "0.0"
        tfNov.text = "0.0"
        tfDec.text = "0.0"
    }

    @FXML
    fun btnSave() {
        val saveExistencias = SaveExistencias(
            clientNumber,
            tfJan.text,
            tfFev.text,
            tfMar.text,
            tfApr.text,
            tfMay.text,
            tfJun.text,
            tfJul.text,
            tfAug.text,
            tfSep.text,
            tfOct.text,
            tfNov.text,
            tfDec.text
        )
        existenciasService.saveExistencias(saveExistencias)
        (tfJan.scene.window as Stage).close()
    }
}