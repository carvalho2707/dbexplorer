package pt.tiagocarvalho.dbexplorerui.controller

import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.Alert.AlertType
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.ComboBox
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.control.TextFormatter
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.util.Callback
import javafx.util.StringConverter
import javafx.util.converter.CurrencyStringConverter
import javafx.util.converter.PercentageStringConverter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import pt.tiagocarvalho.dbexplorerui.conf.ConfigurationProperties
import pt.tiagocarvalho.dbexplorerui.exceptions.DatabaseNotFoundException
import pt.tiagocarvalho.dbexplorerui.exceptions.FileIsLockedException
import pt.tiagocarvalho.dbexplorerui.model.ClientInfo
import pt.tiagocarvalho.dbexplorerui.model.ReportType
import pt.tiagocarvalho.dbexplorerui.model.SearchVO
import pt.tiagocarvalho.dbexplorerui.model.SendReportVO
import pt.tiagocarvalho.dbexplorerui.model.TableRowInfo
import pt.tiagocarvalho.dbexplorerui.services.ExistenciasService
import pt.tiagocarvalho.dbexplorerui.services.LandingService
import pt.tiagocarvalho.dbexplorerui.utils.FormattedTableCellFactory
import pt.tiagocarvalho.dbexplorerui.utils.FormatterUtils.getCurrencyNumberFormat
import pt.tiagocarvalho.dbexplorerui.utils.FormatterUtils.getPercentageNumberFormat
import pt.tiagocarvalho.dbexplorerui.utils.FormatterUtils.intFormatter
import pt.tiagocarvalho.dbexplorerui.utils.Properties.CLIENT_NOT_FOUND
import pt.tiagocarvalho.dbexplorerui.utils.Properties.CLIENT_REQUIRED
import pt.tiagocarvalho.dbexplorerui.utils.isNotZero
import java.awt.Desktop
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate

@Component
class LandingController {

    @Value("classpath:/existencias_dialog.fxml")
    private lateinit var existenciasDialogResource: Resource

    @Autowired
    lateinit var configurationProperties: ConfigurationProperties

    @Autowired
    lateinit var landingService: LandingService

    @Autowired
    lateinit var existenciasService: ExistenciasService

    @FXML
    lateinit var btnSearch: Button

    @FXML
    lateinit var btnPdf: Button

    @FXML
    lateinit var btnXlsx: Button

    @FXML
    lateinit var cbClient: ComboBox<ClientInfo>

    @FXML
    lateinit var tfAno: TextField

    @FXML
    lateinit var tfMes: TextField

    @FXML
    lateinit var tfConta71: TextField

    @FXML
    lateinit var tfConta72a: TextField

    @FXML
    lateinit var tfConta72b: TextField

    @FXML
    lateinit var tfAmort: TextField

    @FXML
    lateinit var tvData: TableView<TableRowInfo>

    @FXML
    lateinit var tcCt: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcDesignacao: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcJanuary: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcFebruary: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcMarch: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcApril: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcMay: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcJune: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcJuly: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcAugust: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcSeptember: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcOctober: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcNovember: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcDecember: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var tcTotal: TableColumn<TableRowInfo, String>

    @FXML
    lateinit var btnEdit: Button

    private var searchDone = false

    companion object {
        private val HUNDRED = BigDecimal("100")
        private const val SHOW_XLSX = false
    }

    @FXML
    fun initialize() {
        setupTables()
        setupInputTypes()
        setupDefaultValues()
        setupListeners()
        setupClientAdapter()
    }

    private fun setupClientAdapter() {
        val clients = landingService.readClients()
        cbClient.items.addAll(clients)
        cbClient.cellFactory = cellFactory

        cbClient.converter = object : StringConverter<ClientInfo>() {
            override fun toString(info: ClientInfo?): String {
                return info?.let {
                    it.code + "  " + it.name
                } ?: ""
            }

            override fun fromString(string: String?): ClientInfo {
                val text = string?.split("  ")
                return when {
                    text != null && text.size >= 2 -> ClientInfo(text[0], text[1])
                    text != null && text.size == 1 -> ClientInfo(text[0], "")
                    else -> ClientInfo("0", "")
                }
            }
        }
    }

    private fun handleCbClientChanges() {
        btnEdit.isVisible = cbClient.value != null && cbClient.value.code.isBlank().not()
        btnSearch.isDisable = !(cbClient.value != null && cbClient.value.code.isBlank().not())
    }

    val cellFactory = Callback<ListView<ClientInfo>, ListCell<ClientInfo>> {
        object : ListCell<ClientInfo>() {
            override fun updateItem(item: ClientInfo?, empty: Boolean) {
                super.updateItem(item, empty)
                item?.let {
                    text = it.code + "  " + it.name
                }
            }
        }
    }

    private fun setupInputTypes() {
        tfAno.textFormatter = intFormatter(LocalDate.now().year)
        tfMes.textFormatter = intFormatter(12)
        tfConta71.textFormatter = TextFormatter(PercentageStringConverter(getPercentageNumberFormat()), 0.00)
        tfConta72a.textFormatter = TextFormatter(PercentageStringConverter(getPercentageNumberFormat()), 0.00)
        tfConta72b.textFormatter = TextFormatter(PercentageStringConverter(getPercentageNumberFormat()), 0.00)
        tfAmort.textFormatter = TextFormatter(CurrencyStringConverter(getCurrencyNumberFormat()), 0.00)
    }

    private fun setupDefaultValues() {
        tfAno.text = LocalDate.now().year.toString()
        tfMes.text = LocalDate.now().monthValue.toString()
    }

    private fun setupListeners() {
        tfAno.textProperty().addListener { _, _, _ ->
            if (searchDone) search()
        }
        tfConta71.textProperty().addListener { _, _, _ ->
            if (searchDone) search()
        }
        tfConta72a.textProperty().addListener { _, _, _ ->
            if (tfConta72a.isNotZero() && tfConta72b.isNotZero()) tfConta72b.text = "0.00%"
            if (searchDone) search()
        }
        tfConta72b.textProperty().addListener { _, _, _ ->
            if (tfConta72b.isNotZero() && tfConta72a.isNotZero()) tfConta72a.text = "0.00%"
            if (searchDone) search()
        }
        tfAmort.textProperty().addListener { _, _, newValue ->
            if (newValue.isEmpty()) tfAmort.text = "0,00 €"
            if (searchDone) search()
        }
        cbClient.valueProperty().addListener { _, _, _ ->
            handleCbClientChanges()
        }
        cbClient.setOnKeyTyped { event ->
            println(event)
        }
        cbClient.setOnKeyPressed { event ->
            if (event.code == KeyCode.ENTER) {
                if (cbClient.value == null || cbClient.value.code.isBlank()) showError(CLIENT_REQUIRED)
                else search()
            }
        }
    }

    private fun setupTables() {
        tcCt.apply {
            setCellValueFactory { param -> param.value.ct }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcDesignacao.apply {
            setCellValueFactory { param -> param.value.designacao }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcJanuary.apply {
            setCellValueFactory { param -> param.value.jan }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcFebruary.apply {
            setCellValueFactory { param -> param.value.feb }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcMarch.apply {
            setCellValueFactory { param -> param.value.mar }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcApril.apply {
            setCellValueFactory { param -> param.value.apr }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcMay.apply {
            setCellValueFactory { param -> param.value.may }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcJune.apply {
            setCellValueFactory { param -> param.value.jun }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcJuly.apply {
            setCellValueFactory { param -> param.value.jul }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcAugust.apply {
            setCellValueFactory { param -> param.value.aug }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcSeptember.apply {
            setCellValueFactory { param -> param.value.sep }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcOctober.apply {
            setCellValueFactory { param -> param.value.oct }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcNovember.apply {
            setCellValueFactory { param -> param.value.nov }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcDecember.apply {
            setCellValueFactory { param -> param.value.dec }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
        tcTotal.apply {
            setCellValueFactory { param -> param.value.tot }
            cellFactory = FormattedTableCellFactory<TableRowInfo, String>()
        }
    }

    @FXML
    fun savePdfReport() {
        val clientNumber = cbClient.value
        if (clientNumber.code.isBlank()) {
            showError(CLIENT_REQUIRED)
            return
        }
        val conta71 = getTfValueWithDefault(tfConta71)
        val conta72a = getTfValueWithDefault(tfConta72a)
        val conta72b = getTfValueWithDefault(tfConta72b)
        val amort = getTfValueWithDefault(tfAmort)
        val sendReportVO = SendReportVO(
            clientNumber.code,
            tfAno.text.toInt(),
            tfMes.text.toInt(),
            conta71,
            conta72a,
            conta72b,
            amort
        )
        try {
            val reportName = landingService.saveReport(sendReportVO, ReportType.PDF)
            showExportedDialog(reportName, ReportType.PDF)
        } catch (file: FileIsLockedException) {
            showError("A acção não pode ser concluída porque o ficheiro está aberto.\nFeche o ficheiro e volte a tentar.")
        } catch (e: Exception) {
            showError(e)
        }
    }

    @FXML
    fun saveXlsxReport() {
        val clientNumber = cbClient.value
        if (clientNumber.code.isBlank()) {
            showError(CLIENT_REQUIRED)
            return
        }
        val conta71 = getTfValueWithDefault(tfConta71)
        val conta72a = getTfValueWithDefault(tfConta72a)
        val conta72b = getTfValueWithDefault(tfConta72b)
        val amort = getTfValueWithDefault(tfAmort)
        val sendReportVO = SendReportVO(
            clientNumber.code,
            tfAno.text.toInt(),
            tfMes.text.toInt(),
            conta71,
            conta72a,
            conta72b,
            amort
        )
        try {
            val reportName = landingService.saveReport(sendReportVO, ReportType.XLSX)
            showExportedDialog(reportName, ReportType.XLSX)
        } catch (file: FileIsLockedException) {
            showError("A acção não pode ser concluída porque o ficheiro está aberto.\nFeche o ficheiro e volte a tentar.")
        } catch (e: Exception) {
            showError(e)
        }
    }

    @FXML
    fun search() {
        searchDone = true
        val clientNumber = cbClient.value
        if (clientNumber.code.isBlank()) {
            showError(CLIENT_REQUIRED)
            return
        }
        val conta71 = getTfValueWithDefault(tfConta71).divide(HUNDRED, 4, RoundingMode.HALF_UP)
        val conta72a = getTfValueWithDefault(tfConta72a).divide(HUNDRED, 4, RoundingMode.HALF_UP)
        val conta72b = getTfValueWithDefault(tfConta72b).divide(HUNDRED, 4, RoundingMode.HALF_UP)
        val amort = getTfValueWithDefault(tfAmort)
        val searchVO = SearchVO(
            clientNumber.code,
            tfAno.text.toInt(),
            tfMes.text.toInt(),
            conta71,
            conta72a,
            conta72b,
            amort
        )
        try {
            val result = landingService.search(searchVO)
            tvData.items = FXCollections.observableArrayList(result.rows)
            Platform.runLater { tvData.refresh() }
            btnPdf.isVisible = true
            btnXlsx.isVisible = SHOW_XLSX
        } catch (dbe: DatabaseNotFoundException) {
            showError(String.format(CLIENT_NOT_FOUND, searchVO.clientNumber))
            searchDone = false
        } catch (e: Exception) {
            showError(e)
        }
    }

    private fun getTfValueWithDefault(textField: TextField): BigDecimal {
        return if (textField.text.isNullOrBlank()) BigDecimal.ZERO
        else textField.text.replace(" ", "").removeSuffix("%").removeSuffix("€").replace(",", "").toBigDecimal()
    }

    private fun showError(message: String) {
        with(Alert(AlertType.ERROR)) {
            title = "Erro"
            headerText = null
            contentText = message
            onCloseRequest
            showAndWait()
        }
    }

    private fun showExportedDialog(reportName: String, type: ReportType) {
        val buttonOpenExplorer = ButtonType("Abrir Localização")

        val alert = Alert(AlertType.INFORMATION).apply {
            title = "Sucesso"
            headerText = null
            contentText = "O mapa de exploração foi gerado com sucesso!\nNome: ${reportName}.${type.extension}"
            buttonTypes.addAll(buttonOpenExplorer)
        }

        val result = alert.showAndWait()
        if (result.isPresent) {
            val buttonClicked = result.get()
            if (buttonClicked == buttonOpenExplorer) {
                Desktop.getDesktop().open(File(configurationProperties.report.path))
            }
        }
    }

    private fun showError(exception: Exception) {
        exception.printStackTrace()
        val alert = Alert(AlertType.ERROR)
        alert.title = "Erro"
        alert.contentText = "Ocorreu um erro inesperado"

        val sw = StringWriter()
        val pw = PrintWriter(sw)
        exception.printStackTrace(pw)
        val exceptionText = sw.toString()

        val label = Label("The exception stacktrace was:")

        val textArea = TextArea(exceptionText).apply {
            isEditable = false
            isWrapText = true
            maxWidth = Double.MAX_VALUE
            maxHeight = Double.MAX_VALUE
        }

        GridPane.setVgrow(textArea, Priority.ALWAYS)
        GridPane.setHgrow(textArea, Priority.ALWAYS)

        val expContent = GridPane().apply {
            maxWidth = Double.MAX_VALUE
            add(label, 0, 0)
            add(textArea, 0, 1)
        }

        alert.dialogPane.expandableContent = expContent
        alert.showAndWait()
    }

    @FXML
    fun btnEditExistencias() {
        val fxmlLoader = FXMLLoader(existenciasDialogResource.url)
        val parent: Parent = fxmlLoader.load()
        val dialogController: ExistenciasController = fxmlLoader.getController()
        dialogController.setInfo(cbClient.value.code, existenciasService)
        dialogController.loadIntoView()
        val scene = Scene(parent, 300.0, 500.0)
        val stage = Stage()
        stage.initModality(Modality.APPLICATION_MODAL)
        stage.scene = scene
        stage.title = cbClient.value.name
        stage.setOnHiding {
            if (searchDone) search()
        }
        stage.showAndWait()
    }

    @FXML
    fun marginKeyPressed(keyEvent: KeyEvent) {
        if (keyEvent.code == KeyCode.ENTER && keyEvent.source is TextField) {
            val source = keyEvent.source as TextField
            when (source.id) {
                "tfConta71" -> tfConta72a.requestFocus()
                "tfConta72a" -> tfConta72b.requestFocus()
                "tfConta72b" -> tfAmort.requestFocus()
            }
        }
    }
}