package pt.tiagocarvalho.dbexplorerui.repository

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class MovCtbRepositoryTest {

    @Test
    fun `conta 312313 should generate id 31`() {
        val expected = "31"
        val result = generateId("312313")
        assertEquals(expected, result)
    }

    @Test
    fun `conta 2722221 should generate id 27`() {
        val expected = "27"
        val result = generateId("2722221")
        assertEquals(expected, result)
    }

    @Test
    fun `conta 321 should generate id 321`() {
        val expected = "321"
        val result = generateId("321")
        assertEquals(expected, result)
    }

    @Test
    fun `conta 334 should generate id 334`() {
        val expected = "334"
        val result = generateId("334")
        assertEquals(expected, result)
    }

    @Test
    fun `conta 75109 should generate id 75`() {
        val expected = "75"
        val result = generateId("75109")
        assertEquals(expected, result)
    }

    private fun generateId(conta: String) = when {
        conta.startsWith("33") -> conta.substring(0, 3)
        conta.startsWith("32") -> conta.substring(0, 3)
        else -> conta.substring(0, 2)
    }
}